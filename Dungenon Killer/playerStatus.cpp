#include "playerStatus.h"

ConsoleHandler* consoleH;
char playerStatusFilePath[] = "playerstatus.data";


playerStatus::playerStatus() {
	ifstream in(playerStatusFilePath, ios::binary); 
	if (in.peek() == ifstream::traits_type::eof()) {
		//file is empty
		levelIndex = 1;
		return;
	}
	if (!in.is_open()) {
		levelIndex = 1;
		return;
	}
	in.read((char*)this, sizeof(playerStatus));
}

playerStatus::~playerStatus() {
	ofstream out(playerStatusFilePath, ios::binary);
	out.write((char*)this, sizeof(playerStatus));
}

inline string playerStatus::LevelFileName(int x) {
	if (consoleH == 0)
		return currentLevelFileName();
	if (x > levelIndex) {
		consoleH->appendToMessage("Level not accessible yet!");
		return currentLevelFileName();
	}
	string s = "";
	s += "res/Level";
	s += x;
	s += ".iv";
	return s.c_str();
}

void playerStatus::setConsoleHandler(ConsoleHandler * p_ch) {
	consoleH = p_ch;
}
