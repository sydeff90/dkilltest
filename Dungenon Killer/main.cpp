#include <iostream>

#include "ConsoleHandler.h"
#include "KeyHandler.h"
#include "playerStatus.h"

using namespace std;

playerStatus* ps;
ConsoleHandler* consoleHandler;

BOOL WINAPI ConsoleHandlerRoutine(DWORD dwCtrlType) {
	if (CTRL_CLOSE_EVENT == dwCtrlType) {
		delete ps;
		delete consoleHandler;
		return TRUE;
	}

	return FALSE;
}

void initializeNewLevel() {
	consoleHandler = new ConsoleHandler(ps->currentLevelFileName().c_str());
	consoleHandler->printMessageWithPauseBetweenCharacters(string("Level 1"));
	ps->setConsoleHandler(consoleHandler);
}

int main(int argc, char* argv[]) {
	SetConsoleCtrlHandler(ConsoleHandlerRoutine, TRUE);
	ps = new playerStatus;
	KeyHandler keyHandler;
	initializeNewLevel();
	while (1) {
		afterDrow:
		switch (keyHandler.getInput()) {
		case KeyHandler::key::ARROW_UP:
			consoleHandler->movePlayer(0, -1);
			break;
		case KeyHandler::key::ARROW_RIGHT:
			consoleHandler->movePlayer(1, 0);
			break;
		case KeyHandler::key::ARROW_LEFT:
			consoleHandler->movePlayer(-1, 0);
			break;
		case KeyHandler::key::ARROW_DOWN:
			consoleHandler->movePlayer(0, 1);
			break;
		case KeyHandler::key::ESC:
			return 0;
		default:
			goto afterDrow;
		}
		if (consoleHandler->levelIsFinished()) {
			consoleHandler->printMessageWithPauseBetweenCharacters(consoleHandler->levelFinalMessage(), false);
			delete consoleHandler;
			ps->incrementPlayerLevel();
			initializeNewLevel();
		}
	}
	return 0;
}