#include "ConsoleHandler.h"

ConsoleHandler::ConsoleHandler(const char * filePath, unsigned short int posX, unsigned short int posY) {
	playerPosition = Position(posX, posY);
	ifstream inFile(filePath);
	readFromFile(inFile);
	v[playerPosition.getX()][playerPosition.getY()] = PLAYER;

	for (int i = 0; i < sizeX; i++)
		for (int j = 0; j < sizeY; j++)
			charV[i][j] = associatedChar(v[i][j]);
	message = "";
	levelFinished = false;
	initializeAllElements(this, elementV, v, sizeX, sizeY, inFile);

	clear();
	draw();
	threadsMustBeStopped = false;
}

ConsoleHandler::ConsoleHandler(unsigned short int sizeX_p, unsigned short int sizeY_p, unsigned short int posX, unsigned short int posY) {
	playerPosition = Position(posX, posY);
	sizeX = sizeX_p;
	sizeY = sizeY_p;

	for (int i = 0; i < sizeX; i++)
		for (int j = 0; j < sizeY; j++)
			v[i][j] = EMPTY;

	//bordare
	for (int i = 0; i < sizeX; i++) {
		v[i][0] = v[i][sizeY - 1] = WALL;
		charV[i][sizeY] = 0;
	}
	for (int i = 0; i < sizeY; i++)
		v[0][i] = v[sizeX - 1][i] = WALL;

	v[playerPosition.getX()][playerPosition.getY()] = PLAYER;

	//completare charV
	for (int i = 0; i < sizeX; i++)
		for (int j = 0; j < sizeY; j++)
			charV[i][j] = associatedChar(v[i][j]);
	message = "";
	levelFinished = false;
	draw();
}

ConsoleHandler::~ConsoleHandler() {
	clear();
	threadsMustBeStopped = true;
	for (int i=0;i<maxSizeX;i++)
		for( int j=0;j<maxSizeY;j++)
			if (elementV[i][j] != 0) {
				delete elementV[i][j];
				elementV[i][j] = 0;
			}
}

void ConsoleHandler::draw(bool printOnlyMessage) {
	if (!printOnlyMessage) {
		for (int i = 0; i < sizeY; i++, cout << endl)
			for (int j = 0; j < sizeX; j++)
				cout << charV[j][i];
		cout << endl;
	}
	cout << message;
	cout << endl;
}

void ConsoleHandler::clear() {
	system("cls");
}

void ConsoleHandler::valueChanged(int posX, int posY) {
	charV[posX][posY] = associatedChar(v[posX][posY]);
	printCharAt(posX, posY, charV[posX][posY]);
}

void ConsoleHandler::valueChanged(Position pos) {
	valueChanged(pos.getX(), pos.getY());
}

char ConsoleHandler::associatedChar(int x) {
	switch (x) {
	case PLAYER:
		return char(1);
	case WALL:
	case ACTIVATABLE_WALL_ACTIVE_NOT_DEFINED_YET:
		return '#';
	case EMPTY:
	case ACTIVATABLE_WALL_INACTIVE_NOT_DEFINED_YET:
		return ' ';
	case LEVEL_FINISH_NOT_DEFINED_YET:
		return '*';
	case WALL_TRIGGER_NOT_DEFINED_YET:
		return '�';
	default:
		return -1;
	};
}

int ConsoleHandler::associatedIndex(char c) {
	switch (c) {
	case '#':
		return WALL;
	case ' ':
		return EMPTY;
	case '@':
		return PLAYER;
	case '*':
		return LEVEL_FINISH_NOT_DEFINED_YET;
	case '\"':
		return ACTIVATABLE_WALL_ACTIVE_NOT_DEFINED_YET;
	case '\'':
		return ACTIVATABLE_WALL_INACTIVE_NOT_DEFINED_YET;
	case '�':
		return WALL_TRIGGER_NOT_DEFINED_YET;
	}
	cerr << "ERROR: UNKNOWN CHARACTER IN INPUTE FILE!!!";
	return -1;
}

void ConsoleHandler::readFromFile(ifstream &in) {
	this->setMessage("Loading Level...");
	this->draw(true);

	if (!in.is_open()) {
		cout << "ERROR: COULD NOT FIND FILE !!!";
		return;
	}
	int x = 0, y = 0, tempinddex;
	char ch, prevChar = 0;
	while (in >> noskipws >> ch) {
		if (ch == ';')
			break;
		if (ch == '\n') {
			y++;
			sizeY = y;
			sizeX = max(sizeX, x);
			x = 0;
			continue;
		}
		tempinddex = associatedIndex(ch);
		if (tempinddex == PLAYER)
			playerPosition = Position(x, y);
		v[x][y] = tempinddex;
		x++;
		
	}
	sizeY++;
}

void ConsoleHandler::movePlayer(int x, int y) {
	int posX, posY;
	posX = playerPosition.getX();
	posY = playerPosition.getY();
	if ((posX + x < 0 || posX + x >= sizeX) || (posY + y < 0 || posY + y >= sizeY)) {
		cout << "ERROR: INDEX OUT OF V!!!\n";
		return;
	}

	if (elementV[posX + x][posY + y] != 0)
		if (elementV[posX + x][posY + y]->isAccessible()) {
			playerPosition.moveBy(x, y);
			v[posX][posY] = elementV[posX][posY]->getElementIndex();
			v[playerPosition.getX()][playerPosition.getY()] = PLAYER;
			elementV[playerPosition.getX()][playerPosition.getY()]->playerTriggeredOn();
			valueChanged(posX, posY);
			valueChanged(playerPosition);
		}
}

void ConsoleHandler::movePlayerAt(Position pos) {
}

void ConsoleHandler::setMessage(string s) {
	message = s;
}

void ConsoleHandler::appendToMessage(string s) {
	message += ' ' + s;
}



void ConsoleHandler::setConsolePos(int x, int y) {
	static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	cout.flush();
	COORD coord = { SHORT(x),SHORT(y) };
	if (!consoleInfoSet)
		if (!GetConsoleScreenBufferInfo(hOut, &SavedConsoleInfo)) {
			cout << "ERROR GETTING PREVIOUS POSITION!!!";
			return;
		}
	consoleInfoSet = true;
	SetConsoleCursorPosition(hOut, coord);
}

void ConsoleHandler::resetConsolePos() {
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), SavedConsoleInfo.dwCursorPosition);
	consoleInfoSet = false;
}

void ConsoleHandler::printCharAt(int x, int y, char c) {
	charPositionChangedMutex.lock();
	setConsolePos(x, y);
	cout << c;
	resetConsolePos();
	charPositionChangedMutex.unlock();
}

void ConsoleHandler::printCharArray(const char* c, int waitTimeMS) {
	string s(c);
	int sizeOfS = s.size();
	char ch;
	for (int i = 0; i < sizeOfS; i++) {
		charPositionChangedMutex.lock();
		cout << s[i];
		charPositionChangedMutex.unlock();
		if (threadsMustBeStopped)
			goto EXIT;
		Sleep(waitTimeMS);
		if (threadsMustBeStopped)
			goto EXIT;
	}
EXIT:
	cout << endl;
}
