#include "BaseElement.h"

#include "ConsoleHandler.h"

ConsoleHandler* parent;

void initializeAllElements(ConsoleHandler* consoleH, 
	BaseElement* elementV[maxSizeX][maxSizeY], 
	short int v[maxSizeX][maxSizeY],
	unsigned short int x,
	unsigned short int y,
	ifstream& in) {
	parent = consoleH;
	for (int i = 0; i < maxSizeX; i++)
		for (int j = 0; j < maxSizeY; j++)
			elementV[i][j] = 0;

	queue<WallTriggerElement*> notInitializedTriggers;

	for (int i = 0; i < x; i++)
		for (int j = 0; j < y; j++) {
			switch (v[i][j]) {
			case ConsoleHandler::EMPTY:
				elementV[i][j] = new EmptyElement(i, j, ConsoleHandler::EMPTY);
				break;
			case ConsoleHandler::WALL:
				elementV[i][j] = new WallElement(i, j, ConsoleHandler::WALL);
				break;
			case ConsoleHandler::WALL_TRIGGER_NOT_DEFINED_YET:
				elementV[i][j] = new WallTriggerElement(i, j, ConsoleHandler::WALL_TRIGGER_NOT_DEFINED_YET);
				notInitializedTriggers.push((WallTriggerElement*)elementV[i][j]);
				break;
			case ConsoleHandler::ACTIVATABLE_WALL_ACTIVE_NOT_DEFINED_YET:
				elementV[i][j] = new ActivatableWallElement(false, i, j, ConsoleHandler::ACTIVATABLE_WALL_ACTIVE_NOT_DEFINED_YET);
				break;
			case ConsoleHandler::ACTIVATABLE_WALL_INACTIVE_NOT_DEFINED_YET:
				elementV[i][j] = new ActivatableWallElement(true, i, j, ConsoleHandler::ACTIVATABLE_WALL_INACTIVE_NOT_DEFINED_YET);
				break;
			case ConsoleHandler::LEVEL_FINISH_NOT_DEFINED_YET:
				elementV[i][j] = new LevelFinishElement(i, j, ConsoleHandler::LEVEL_FINISH_NOT_DEFINED_YET);
				break;
			case ConsoleHandler::PLAYER:
				elementV[i][j] = new EmptyElement(i, j, ConsoleHandler::EMPTY);
				break;
			}
		}
	while (!notInitializedTriggers.empty()) {
		notInitializedTriggers.front()->initialize(in);
		notInitializedTriggers.pop();
	}
}

void WallTriggerElement::initialize(ifstream& in) {
	int n, x1, y1;
	ActivatableWallElement* currentWallElement;
	in >> skipws >> n;
	for (int i = 0; i < n; i++) {
		in >> x1 >> y1;
		
		currentWallElement = (ActivatableWallElement*)parent->getPointerAtElement(x1, y1);
		wallsToBeActivated.push_back(currentWallElement);
	}
}

void LevelFinishElement::playerTriggeredOn() {
	parent->levelEnded();
}

void ActivatableWallElement::changeAccessibleValue() {
	accessible = !accessible;
	if (accessible) {
		parent->changeElement(x, y, ConsoleHandler::ACTIVATABLE_WALL_INACTIVE_NOT_DEFINED_YET);
		elementIndex = ConsoleHandler::ACTIVATABLE_WALL_INACTIVE_NOT_DEFINED_YET;
	}
	else {
		parent->changeElement(x, y, ConsoleHandler::ACTIVATABLE_WALL_ACTIVE_NOT_DEFINED_YET);
		elementIndex = ConsoleHandler::ACTIVATABLE_WALL_ACTIVE_NOT_DEFINED_YET;
	}
}