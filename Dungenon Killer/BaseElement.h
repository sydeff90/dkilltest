#pragma once

#include <vector>
#include <queue>

#include "MaximumValues.h"

using namespace std;

class ConsoleHandler;

class BaseElement {
public:
	int x, y, elementIndex;
	BaseElement(int _x, int _y, int elementind) { x = _x; y = _y; elementIndex = elementind; }
	~BaseElement() { }
	virtual bool isAccessible() { return true; }
	virtual void playerTriggeredOn() { }
	virtual void playerTriggeredNearby() { }
	virtual bool canBeTriggeredNearby() { return false; }
	virtual int getElementIndex() { return 0; }
};

class WallElement : public BaseElement {
public:
	WallElement(int x,int y, int elementindex) : BaseElement(x,y, elementindex) { }
	bool isAccessible() { return false; }
	int getElementIndex() { return elementIndex; }
};

class EmptyElement : public BaseElement { 
public:
	EmptyElement(int x, int y, int elementindex) : BaseElement(x, y, elementindex) { }
	int getElementIndex() { return elementIndex; }
};

class LevelFinishElement : public BaseElement {
public:
	LevelFinishElement(int x, int y, int elementindex) : BaseElement(x, y, elementindex) { }
	void playerTriggeredOn();
	int getElementIndex() { return elementIndex; }
};

class ActivatableWallElement : public BaseElement {
	bool accessible;
public:
	ActivatableWallElement(bool isAccessible, int x, int y, int elementindex) 
		: BaseElement(x, y, elementindex) { accessible = isAccessible; }
	bool isAccessible() { return accessible; }
	void changeAccessibleValue();
	int getElementIndex() { return elementIndex; }
};

class WallTriggerElement : public BaseElement {
	vector <ActivatableWallElement*> wallsToBeActivated;
public:
	WallTriggerElement(int x, int y, int elementindex) : BaseElement(x, y, elementindex) { }
	void initialize(ifstream& in);
	void playerTriggeredOn() {
		for (int i = 0; i < wallsToBeActivated.size(); i++)
			wallsToBeActivated[i]->changeAccessibleValue();
	};
	int getElementIndex() { return elementIndex; }
};

void initializeAllElements(
	ConsoleHandler* consoleH,
	BaseElement* elementV[maxSizeX][maxSizeY],
	short int v[maxSizeX][maxSizeY],
	unsigned short int x,
	unsigned short int y,
	ifstream& in
);