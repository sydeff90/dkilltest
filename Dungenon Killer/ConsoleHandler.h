#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <thread>
#include <mutex>
#include <Windows.h>

#include "MaximumValues.h"
#include "BaseElement.h"

using namespace std;

struct Position
{
private:
	unsigned short int x, y;
public:
	Position(const unsigned short int x, const unsigned int y):x(x),y(y) {}
	Position() {}
	inline void setX(int x) { this->x = x; }
	inline void setY(int y) { this->y = y; }
	inline unsigned short int& getX() { return x; }
	inline unsigned short int& getY() { return y; }
	inline void moveBy(int xVal, int yVal) {
		x += xVal;
		y += yVal;
	}
};

class ConsoleHandler {
private:
	int sizeX, sizeY;
	char charV[maxSizeX][maxSizeY + 1];
	BaseElement* elementV[maxSizeX][maxSizeY];
	short int v[maxSizeX][maxSizeY];
	Position playerPosition;
	string message, levelEndMessage="Level Finished!";
	CONSOLE_SCREEN_BUFFER_INFO SavedConsoleInfo;
	bool consoleInfoSet = false;
	bool levelFinished = false;
	bool threadsMustBeStopped;
	mutex charPositionChangedMutex;
	
	
	enum Colors {
		
	};

	void setConsolePos(int x, int y);
	void resetConsolePos();
public:
	enum {
		EMPTY=0,
		PLAYER,
		WALL,
		LEVEL_FINISH_NOT_DEFINED_YET,
		WALL_TRIGGER_NOT_DEFINED_YET,
		ACTIVATABLE_WALL_ACTIVE_NOT_DEFINED_YET,
		ACTIVATABLE_WALL_INACTIVE_NOT_DEFINED_YET,

		ENUM_SIZE
	};
	~ConsoleHandler();
	ConsoleHandler(const char* filePath, unsigned short int posX = 1, unsigned short int posY = 1);
	ConsoleHandler(unsigned short int sizeX, unsigned short int sizeY, unsigned short int posX = 1, unsigned short int posY = 1);
	void draw(bool printOnlyMessage = false);
	void clear();
	char associatedChar(int x);
	int associatedIndex(char c);
	void readFromFile(ifstream &in);
	void movePlayer(int x, int y);
	void movePlayerAt(Position pos);
	void setMessage(string s);
	void appendToMessage(string s);
	void printCharAt(int x, int y, char c);
	void printCharArray(const char* c,int waitTimeMs = 0);
	void valueChanged(int posX, int posY);
	void valueChanged(Position pos);
	
	inline void printMessageWithPauseBetweenCharacters(string s, bool writeInSeparateThread = true) { 
		if (writeInSeparateThread) {
			thread messageThread(&ConsoleHandler::printCharArray, this, s.c_str(), 60); 
			messageThread.detach(); 
			return;
		}
		printCharArray(s.c_str(), 60);
	}
	inline string levelFinalMessage() { return levelEndMessage; }
	inline void changeElement(int posX, int posY, int typeIndex) { v[posX][posY] = typeIndex; valueChanged(posX, posY); }
	inline void* getPointerAtElement(int x, int y) { return this->elementV[x][y]; }
	inline bool levelIsFinished() { return levelFinished; }
	inline void levelEnded() { levelFinished = true; }
	inline void clearMessage() { message = ""; setMessage("Level Finished"); draw(true); }
};