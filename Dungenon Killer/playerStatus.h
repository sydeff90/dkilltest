#pragma once
#include <string>
#include <fstream>

#include "ConsoleHandler.h"

using namespace std;

class playerStatus
{
private:
	int levelIndex;
public:
	playerStatus(); 
	~playerStatus(); 
	inline void incrementPlayerLevel() {
		if (levelIndex < getMaxLevelValue()) levelIndex++; }
	inline string currentLevelFileName() { 
		string s = "";
		s += "Level";
		s += to_string(levelIndex);
		s += ".iv";
		return s;
	}
	inline string LevelFileName(int x);
	void setConsoleHandler(ConsoleHandler* p_ch);
};

